export default async function Head() {
  return (
    <>
      <title>Alma Soul</title>
      <meta content="width=device-width, initial-scale=1" name="viewport" />
      <link rel="icon" href="/favicon.ico" />
    </>
  );
}
