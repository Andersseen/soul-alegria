import "./global.css";
import Providers from "./providers";
import { LINKS } from "../data/links";
import { Logo } from "../components/logo";
import { Roboto } from "next/font/google";
import { WrapWithFooter } from "@/components/client/wrapper/wrap-with-footer";
import { ThemeSwitcher } from "@/components/theme-switcher";
import { HeaderNavigation } from "@/components/header-navigation";

const roboto = Roboto({
  weight: ["400", "700"],
  style: ["normal", "italic"],
  subsets: ["latin"],
  display: "swap",
});

export const metadata = {
  title: "Soul Alegría",
  description: "Cultura y arte para todos los oídos",
  keywords: [
    "Brand, Publicidad, Trading,Trade,Agencia, Logistics, Proyecto social",
  ],
  authors: [
    {
      name: "Andersseen",
    },
  ],
  creator: "Andersseen",
  publisher: "Andersseen",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="es">
      <head />
      <body className={roboto.className}>
        <Providers>
          <HeaderNavigation
            navLinks={LINKS}
            logo={<Logo />}
            themeSwitcher={<ThemeSwitcher />}
          />
          <WrapWithFooter>
            <main>{children}</main>
          </WrapWithFooter>
        </Providers>
      </body>
    </html>
  );
}
