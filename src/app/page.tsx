import { PageWrapperWithTransition } from "@/components/client/wrapper/new-page-wrapper";
import Hero from "../components/client/page/hero";
import { HeroVideoDialog } from "@/components/hero-video-dialog";

export default function Page() {
  return (
    <PageWrapperWithTransition>
      <HeroVideoDialog
        animationStyle="top-in-bottom-out"
        videoSrc="https://www.youtube.com/embed/Q_OpUZSGlT4?si=49ACKL7QNH5M_SuR"
        thumbnailAlt="Hero Video"
      />
      <Hero />
    </PageWrapperWithTransition>
  );
}
