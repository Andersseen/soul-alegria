import { compileMDX } from "next-mdx-remote/rsc";
import { promises as fs } from "fs";
import path from "path";
import { PageWrapperWithTransition } from "@/components/client/wrapper/new-page-wrapper";


export default async function Page({ params }: { params: { post: string } }) {
  const content = await fs.readFile(
    path.join(process.cwd(), "src/data/posts", `${params.post}.mdx`),
    "utf-8"
  );

  const data = await compileMDX<any>({
    source: content,
    options: {
      parseFrontmatter: true,
    },
  });

  return (
    <PageWrapperWithTransition>
      <article className="max-w-4xl mx-auto px-4 py-8">
        <header className="mb-8">
          <h1 className="text-4xl font-bold mb-4">{data.frontmatter.title}</h1>
        </header>

        <img
          className="w-full object-cover shadow-lg mb-8"
          src={data.frontmatter.imageUrl}
          alt={data.frontmatter.title}
        />

        <div className="prose prose-lg max-w-none">{data.content}</div>
      </article>
    </PageWrapperWithTransition>
  );
}
