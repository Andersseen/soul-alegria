import { PageWrapperWithTransition } from "@/components/client/wrapper/new-page-wrapper";
import { promises as fs } from "fs";
import path from "path";
import { compileMDX } from "next-mdx-remote/rsc";
import BlogCard from "@/components/blog-card";

interface Post {
  id: number;
  title: string;
  date: string;
  imageUrl: string;
  content: string;
}

export default async function Page() {
  // const posts = await getPosts();
  const filenames = await fs.readdir(
    path.join(process.cwd(), "src/data/posts")
  );

  const posts = await Promise.all(
    filenames.map(async (filename) => {
      const content = await fs.readFile(
        path.join(process.cwd(), "src/data/posts", filename),
        "utf-8"
      );
      const { frontmatter } = await compileMDX<Post>({
        source: content,
        options: {
          parseFrontmatter: true,
        },
      });
      return {
        filename,
        slug: filename.replace(".mdx", ""),
        ...frontmatter,
      };
    })
  );

  return (
    <PageWrapperWithTransition>
      <section className="min-h-screen sm:p-2 lg:p-4">
        <div className="grid grid-cols-12 gap-4 px-8 mt-6">
          {posts.map((item) => (
            <BlogCard key={item.id} post={item} />
          ))}
        </div>
      </section>
    </PageWrapperWithTransition>
  );
}
