import { Contact, FormContact } from "@/components/client/page/contact";
import { PageWrapperWithTransition } from "@/components/client/wrapper/new-page-wrapper";

export default function Page() {
  return (
    <PageWrapperWithTransition>
      <Contact>
        <FormContact />
      </Contact>
    </PageWrapperWithTransition>
  );
}
