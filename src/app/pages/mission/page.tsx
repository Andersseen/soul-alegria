import { PageWrapperWithTransition } from "@/components/client/wrapper/new-page-wrapper";
import { CheckedIcon } from "@/components/icons/check";
import { LightWrapper } from "@/components/light-wrapper";
import { PageWithHeader } from "@/components/page-content";
import { HeaderData } from "@/utils";

export default function Page() {
  const headerContent: HeaderData = {
    srcImg: "/9.jpg",
    title: "Soul Alegria",
    subtitle: "NUESTRA MISIÓN",
    pages: {
      prevPage: "INICIO",
      currentPage: "NUESTRA MISIÓN",
    },
  };

  const featureTexts = [
    "Escuchar a nuestros clientes para ofrecer soluciones proactivas, realistas y únicas.",
    "ROI A+ es nuestro ADN y con nuestras herramientas innovadoras, podrá conseguir más con el mismo presupuesto. ",
    "Optimizar sus campañas offline/online con calidad/precios “low cost” donde llevamos planificando y comprando medios desde 1999. ",
    "ROI A+ porque le ofrecemos planteamientos innovadores de su inversión publicitaria, crear acciones sociales únicas y/o gestionar eventos ad-hoc, que podrá pagar parte con su producto.",
    "Agencia Boutique con una relación única con los medios de comunicación (cubrimos sus necesidades de eventos, acciones promocionales, concursos, flota, electrónica, medios, etc.) a cambio de condiciones ventajosas.",
    "Especialización en Medios Exteriores, Programática y Re-Targeting con tarifas muy competitivas.​Le ofrecemos nuevas salidas a su producto tanto nuevos, stock, baja rotación, slow movers,  sobreproducción, etc. a nuevos mercados (nacional e internacional).",
    "Trabajamos en toda Europa.",
  ];

  return (
    <PageWrapperWithTransition>
      <PageWithHeader headerData={headerContent}>
        <LightWrapper>
          <section className="min-w-screen px-4 py-8 sm:py-12 sm:px-6 lg:py-16 lg:px-8">
            <div className="max-w-xl">
              <h2 className="text-3xl font-bold sm:text-4xl">
                Que sabemos hacer:
              </h2>
            </div>

            <div className="mt-8 lg:mx-16 grid grid-cols-1 gap-8 md:mt-16">
              {featureTexts.map((text, index) => (
                <Feature key={index}>{text}</Feature>
              ))}
            </div>
          </section>
        </LightWrapper>
      </PageWithHeader>
    </PageWrapperWithTransition>
  );
}

const Feature = ({ children }: { children: React.ReactNode }) => (
  <div className="flex items-start gap-4">
    <div className="shrink-0">
      <CheckedIcon />
    </div>
    <h3 className="text-lg font-semibold">{children}</h3>
  </div>
);
