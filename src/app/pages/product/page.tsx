import { PageWrapperWithTransition } from "@/components/client/wrapper/new-page-wrapper";
import { CheckedIcon } from "@/components/icons/check";
import { LightWrapper } from "@/components/light-wrapper";
import { PageWithHeader } from "@/components/page-content";
import { HeaderData } from "@/utils";

export default function Page() {
  const headerContent: HeaderData = {
    srcImg: "/10.jpg",
    title: "Soul Alegria",
    subtitle: "PRODUCTOS Y SERVICIOS",
    pages: {
      prevPage: "INICIO",
      currentPage: "PRODUCTOS Y SERVICIOS",
    },
  };
  const featureTexts = [
    "Servicio de marketing personalizado, atención y dedicación plena a nuestros clientes con nuestra agencia boutique, que sabe de planificación y negociación en medios, ha creado eventos de primer orden y realiza proyectos sociales únicos  (último proyecto Museo Thyssen - Bankinter - signo guía para la comunidad de hipoacúsicos).",
    "Nuevas ventas a nuevos mercados identificados. Ofrecemos mercados de compradores finales tanto nacionales como internacionales que le da valor y salida a su producto.",
    "Herramientas financieras internacionalmente reconocidas para la mejora de su productividad como es el Media Credit, Smart Trading, Full Barter, Comercio Inteligente todo para mejorar su ROI que se convierte en ROI A+.",
  ];
  return (
    <PageWrapperWithTransition>
      <PageWithHeader headerData={headerContent}>
        <LightWrapper>
          <section className="min-w-screen px-4 py-8 sm:py-12 sm:px-6 lg:py-16 lg:px-8">
            <div className="max-w-xl">
              <h2 className="text-3xl font-bold sm:text-4xl">Que ofrecemos:</h2>
            </div>

            <div className="mt-8 lg:mx-16 grid grid-cols-1 gap-8 md:mt-16">
              {featureTexts.map((text, index) => (
                <Feature key={index}>{text}</Feature>
              ))}
            </div>
          </section>
        </LightWrapper>
      </PageWithHeader>
    </PageWrapperWithTransition>
  );
}

const Feature = ({ children }: { children: React.ReactNode }) => (
  <div className="flex items-start gap-4">
    <div className="shrink-0">
      <CheckedIcon />
    </div>
    <h3 className="text-lg font-semibold">{children}</h3>
  </div>
);
