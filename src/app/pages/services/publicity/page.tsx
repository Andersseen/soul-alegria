import { Publicity } from "@/components/client/page/publicity";
import { PageWrapperWithTransition } from "@/components/client/wrapper/new-page-wrapper";
import { PageContent } from "@/components/page-content";
import { HeaderData, TextContentData } from "@/utils";

export default function Page() {
  const headerContent: HeaderData = {
    srcImg: "/7.jpg",
    title: "Soul Alegria",
    subtitle: "PUBLICIDAD",
    pages: {
      prevPage: "INICIO",
      currentPage: "PUBLICIDAD",
    },
  };

  const texteContent: TextContentData = {
    title: `Servicio de marketing personalizado,`,
    paragraphs: [
      `Servicio de marketing personalizado, atención y dedicación plena a nuestros clientes con nuestra agencia boutique, que sabe de planificación y negociación en medios, ha creado eventos de primer orden y realiza proyectos sociales únicos  (último proyecto Museo Thyssen - Bankinter - signo guía para la comunidad de hipoacúsicos).
Optimizar sus campañas offline/online con calidad/precios “low cost” donde llevamos planificando y comprando medios desde 1999.
ROI A+ porque le ofrecemos planteamientos innovadores de su inversión publicitaria, crear acciones sociales únicas y/o gestionar eventos ad-hoc, que podrá pagar parte con su producto.`,
    ],
  };
  return (
    <PageWrapperWithTransition>
      <PageContent headerData={headerContent} textContentData={texteContent} />
      <Publicity />
    </PageWrapperWithTransition>
  );
}
