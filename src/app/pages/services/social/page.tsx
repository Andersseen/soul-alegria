"use client";
import { Image } from "@nextui-org/react";
import { PageWrapperWithTransition } from "@/components/client/wrapper/new-page-wrapper";
import { PageWithHeader } from "@/components/page-content";
import { TextContent } from "@/components/text-content";
import { HeaderData, TextContentData } from "@/utils";

export default function Page() {
  const headerContent: HeaderData = {
    srcImg: "/3.jpg",
    title: "Soul Alegria",
    subtitle: "PROYECTO SOCIAL",
    pages: {
      prevPage: "INICIO",
      currentPage: "PROYECTO SOCIAL",
    },
  };
  const textContent: TextContentData = {
    title: `Situación`,
    paragraphs: [
      `La deficiencia auditiva es una de las discapacidades que menos se suele ver La
    escasez y antiguedad de los datos en referencia a las personas con cualquier tipo
    de pérdida auditiva en nuestro país es sintomático de ello
    `,
      `La mayoría de sistemas implementados en la actualidad para mejorar la
    accesibilidad de personas con deficiencia auditiva se centran en 2 tipos: los bucles
    magnéticos la traducción a lengua de signos (LSE).`,

      `Ambas son herramientas necesarias pero que no garantizan el derecho de
    accesibilidad a nuestra comunidad al completo.`,
    ],
  };
  const secondTextContent: TextContentData = {
    title: `Mision`,
    paragraphs: [
      `Soul & Alegria ayuda a integrar
      en la sociedad a las personas
      con dificultad auditiva, los
      hipoacúsicos y mejorar su
      calidad de vida a través del arte
      y la cultura`,
    ],
  };
  return (
    <PageWrapperWithTransition>
      <PageWithHeader headerData={headerContent}>
        <TextContent textContentData={textContent} flipSVG={true} />
        <TextContent textContentData={secondTextContent} />
        <section className="sm:grid sm:grid-cols-2">
          <div className="p-8 md:p-12 lg:px-16 lg:py-24">
            <div className="mx-auto max-w-xl text-center">
              <h2 className="text-2xl font-bold md:text-3xl">HIPOACUSIA</h2>
              <div className="text-left">
                <p className="md:mt-4 md:block">
                  A partir de 20 decibelios de pérdida auditiva se considera
                  hipoacusia.
                </p>
                <p className="md:mt-4 md:block">
                  A partir de 40 decibelios es discapacidad auditiva.
                </p>
                <p className="md:mt-4 md:block">
                  Es decir toda persona que tiene dificultades auditivas es
                  hipoacúsica, pero no sorda completa y no sabe lengua de
                  signos.
                </p>
              </div>
            </div>
          </div>

          <Image alt="baby" src="/13.jpg" />
        </section>
        <h6>
          7.000.000 de personas en España sufren pérdida auditiva y un 11% de la
          población mundial, según la Organización Mundial de la Salud.
        </h6>
        <section className="mt-32 mb-16 overflow-hidden sm:grid sm:grid-cols-2">
          <Image alt="baby" src="/14.jpg" />
          <div className="p-8 md:p-12 lg:px-16 lg:py-24">
            <div className="mx-auto max-w-xl text-center">
              <h2 className="text-2xl font-bold md:text-3xl">Caso de exito</h2>
              <div className="text-left">
                <p className="md:mt-4 md:block">
                  Soul&Alegria crea un proyecto de signoguia con subtitulado
                  dirigido a los hipoacúsicos con el mecenazgo de
                  <span className="text-main-400"> BANKINTER </span>
                  para el
                  <span className="text-main-400">
                    {" "}
                    MUSEO NACIONAL THYSSEN{" "}
                  </span>
                  .
                </p>
              </div>
            </div>
          </div>
        </section>
      </PageWithHeader>
    </PageWrapperWithTransition>
  );
}
