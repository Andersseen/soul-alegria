import { Trading, TradingSection } from "@/components/client/page/trading";
import { PageWrapperWithTransition } from "@/components/client/wrapper/new-page-wrapper";
import { PageContent } from "@/components/page-content";
import { HeaderData, TextContentData } from "@/utils";

export default function Page() {
  const headerContent: HeaderData = {
    srcImg: "/8.jpg",
    title: "Soul Alegria",
    subtitle: "TRANDING & LOGISTICS",
    pages: {
      prevPage: "INICIO",
      currentPage: "TRANDING & LOGISTICS",
    },
  };

  const texteContent: TextContentData = {
    title: `Expertos en Producto`,
    paragraphs: [
      `Todo bien y creo que habria que poner los sectores que mas tocamos como es Automovil (VI, VO, VC km 0, etc), Bebidas (Spirit & Vino), Lujo (Relojeria, Joyeria, Bisuteria Lujo), Electrodomesticos,  … FMCG y el sector de Medios de Comunicacion.`,
      `Tendro de Trade hemos creado una MA Inspirium (MAI) es un mayorista centrado en últimas tendencias, especializado en la importación de marcas nacionales e internacionales, marca blanca y materia prima relacionados con el mundo del cannabis medicinal, la cosmética, el cuidado personal, el deporte, la salud, el ocio, las mascotas y el bienestar sexual www.mainspirium.com`,
    ],
  };
  const headOfContent = `COMO ES SOUL & ALEGRIA - TRADING & LOGISTICS?`;
  const bodyOfContent = [
    "Expertos en importación, exportación y venta de producto",
    "Partner de primeras marcas para dar salida a su producto sin intervenir en su canal de venta.",
    "Gestión de producto desclasificados, descatalogados, stocks,... en nuevos mercados.",
    "Relación nacional e internacional con clientes finales (compra y venta).",
    "Soluciones logísticas integrales.",
    "Transporte nacional e internacional.",
    "Instalaciones propias de 30.000m2.",
  ];
  return (
    <PageWrapperWithTransition>
      <PageContent headerData={headerContent} textContentData={texteContent} />
      <TradingSection />
      <Trading head={headOfContent} body={bodyOfContent} />
    </PageWrapperWithTransition>
  );
}
