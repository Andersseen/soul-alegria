"use client";
import { Card, Image, CardFooter, Button } from "@nextui-org/react";
import Link from "next/link";

export default function BlogCard({ post }: any) {
  return (
    <Card className="col-span-12 md:col-span-6 lg:col-span-4 xl:col-span-3">
      <Image
        removeWrapper
        className="z-0 w-full h-full object-cover"
        src={post.imageUrl}
        alt={post.title}
      />
      <CardFooter className="absolute bg-black/50 bottom-0 z-10 border-t-1 border-default-600 dark:border-default-100 flex justify-between">
        <div className="flex flex-col gap-2">
          <h6 className="text-bold text-white/80 text-base">{post.title}</h6>
        </div>
        <Button
          className="bg-gradient-to-tr from-pink-500 to-yellow-500 text-white shadow-lg"
          radius="full"
          size="sm"
          as={Link}
          href={`blog/${post.slug}`}
        >
          Leer más
        </Button>
      </CardFooter>
    </Card>
  );
}
