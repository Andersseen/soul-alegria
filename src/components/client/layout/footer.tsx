'use client';
import React, { forwardRef, useEffect, useRef, useState } from 'react';
import { motion } from 'framer-motion';
import Link from 'next/link';
import { Button } from '@nextui-org/react';

const links = [
  { label: 'INICIO', route: '/' },
  { label: 'NOSOTROS', route: '/pages/about' },
  { label: 'MISIÓN', route: '/pages/mission' },
  { label: 'PRODUCTOS Y SERVICIOS', route: '/pages/product' },
  { label: 'BLOG', route: '/pages/blog' },
  { label: 'CONTACTO', route: '/pages/contact' },
];

interface FooterProps {
  isVisible: boolean;
}

const Footer = forwardRef(function FooterRef(
  { isVisible }: FooterProps,
  forwardedRef: React.Ref<HTMLDivElement>
) {
  const [footerHeight, setFooterHeight] = useState(0);
  const footerRef = useRef<HTMLDivElement | null>(null);
  const year = new Date().getFullYear();

  useEffect(() => {
    if (footerRef.current) {
      setFooterHeight(footerRef.current.offsetHeight);
    }
  }, []);

  return (
    <>
      <div
        ref={forwardedRef}
        style={{ height: footerHeight }}
        className="w-full h-1"
      />
      <motion.footer
        initial={{ opacity: 0, y: '110%' }}
        animate={isVisible ? { opacity: 1, y: 0 } : { opacity: 0, y: '100%' }}
        ref={footerRef}
        className="fixed bottom-0 left-0 z-0 w-full h-fit bg-red-400/80"
      >
        <section className="p-4  md:p-8 lg:p-10">
          <div className="mx-auto max-w-screen-xl text-center">
            <motion.div
              initial={{ x: -100, rotate: -45, scale: 0 }}
              animate={
                isVisible
                  ? { x: 0, rotate: 0, scale: 1 }
                  : { x: -100, rotate: -45, scale: 0 }
              }
            >
              <p>PARA MÁS INFORMACIÓN</p>
              <p>
                Contáctanos, cuéntanos tus experiencias, intereses y
                necesidades:
              </p>
              <p>Info@soulalegria.com</p>
              <ul className="flex flex-wrap justify-center items-center mb-6 text-gray-900 dark:text-white">
                {links.map((value, index) => (
                  <Button key={`${value.label}-${index}`} variant="light">
                    <Link href={value.route}>{value.label}</Link>
                  </Button>
                ))}
              </ul>
              ©{year} Soul & Alegría.
            </motion.div>
          </div>
        </section>
        <div className="w-full h-full text-center pt-3"></div>
      </motion.footer>
    </>
  );
});

export default Footer;
