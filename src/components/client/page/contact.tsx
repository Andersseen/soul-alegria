'use client';
import { motion, mirrorEasing } from 'framer-motion';
import { useTheme } from 'next-themes';
import { Input } from '@nextui-org/react';
import { Textarea } from '@nextui-org/react';
import { Button } from '@nextui-org/react';

export const Contact = ({ children }: { children: React.ReactNode }) => {
  const { resolvedTheme } = useTheme();
  const filters = {
    light: 'rgba(255,255,255,1)',
    dark: 'rgba(0,0,0,1)',
  };
  const easeIn = (progress) => progress * progress;

  const easeInOut = mirrorEasing(easeIn);

  const themeFilters = filters[resolvedTheme] || filters.light;
  return (
    <div className="relative min-h-screen">
      <div className="fixed top-0 left-0 w-full h-full opacity-50">
        <motion.div
          className="w-full h-full"
          animate={{
            backgroundImage: [
              `radial-gradient(circle, rgba(248,113,113,.1) 0%, ${themeFilters} 10%, ${themeFilters} 100%)`,
              `radial-gradient(circle, ${themeFilters} 0%, rgba(248,113,113,.1)50%, ${themeFilters} 100%)`,
              `radial-gradient(circle, ${themeFilters} 0%, ${themeFilters} 90%, rgba(248,113,113,.1) 100%)`,
            ],
          }}
          transition={{
            duration: 5,
            repeat: Infinity,
            repeatType: 'reverse',
            ease: easeInOut,
          }}
        ></motion.div>
      </div>

      <div className="relative z-10 p-8">{children}</div>
    </div>
  );
};

export const FormContact = () => {
  return (
    <section>
      <div className="py-8 lg:py-16 px-4 mx-auto max-w-screen-md">
        <h2 className="mb-4 text-4xl tracking-tight font-extrabold text-center">
          CONTÁCTANOS
        </h2>
        <p className="font-light text-center sm:text-xl">
          Llámanos al tlf: +34 610 424 327
        </p>
        <p className="font-light text-center sm:text-xl">
          Mándanos un email a: Info@soulalegria.com
        </p>
        <p className="mb-4 lg:mb-8  font-light text-center sm:text-xl">
          O rellena nuestro formulario de contacto:
        </p>
        <form action="#" className="space-y-8">
          <div className="flex w-full flex-wrap md:flex-nowrap gap-4">
            <Input
              variant="bordered"
              type="text"
              label="Nombre y apellido"
              placeholder="Escribe aqui su nombre completo"
            />
          </div>
          <div className="flex w-full flex-wrap md:flex-nowrap gap-4">
            <Input
              variant="bordered"
              type="email"
              label="Email"
              placeholder="Escribe aqui su email"
            />
          </div>
          <div className="flex w-full flex-wrap md:flex-nowrap gap-4">
            <Textarea
              variant="bordered"
              label="Mensaje"
              labelPlacement="outside"
              placeholder="Escribe aqui su mensaje"
            />
          </div>
          <Button color="secondary" variant="bordered">
            Enviar
          </Button>
        </form>
      </div>
    </section>
  );
};
