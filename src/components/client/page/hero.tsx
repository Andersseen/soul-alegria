"use client";
import React from "react";
import { AnimatePresence } from "framer-motion";
import BackgroundImage from "../../ui/background-image";
import SlideInfo from "../../ui/slide-info";
import Slides from "../../ui/slides";
import Controls from "../../ui/controls";
import { SliderItem, SLIDES as sliderData } from "@/data/slides";

export default function Hero() {
  const [data, setData] = React.useState<SliderItem[]>(sliderData.slice(1));
  const [transitionData, setTransitionData] = React.useState<SliderItem>(
    sliderData[0]
  );
  const [currentSlideData, setCurrentSlideData] = React.useState({
    data: initData,
    index: 0,
  });

  return (
    <section className="relative min-h-screen select-none overflow-hidden text-white antialiased">
      <AnimatePresence>
        <BackgroundImage
          key={transitionData.id}
          transitionData={transitionData}
          currentSlideData={currentSlideData}
        />

        <div className="absolute z-20 h-full w-full">
          <div className="flex h-full w-full grid-cols-10 flex-col md:grid">
            <div className="col-span-5 mb-3 flex h-full flex-1 flex-col justify-end px-5 md:mb-0 md:justify-center md:px-10">
              <SlideInfo
                transitionData={transitionData}
                currentSlideData={currentSlideData}
              />
            </div>
            <div className="col-span-5 flex h-full flex-1 flex-col justify-start p-4 md:justify-center md:p-10">
              <Slides data={data} />
              <Controls
                currentSlideData={currentSlideData}
                data={data}
                transitionData={transitionData}
                initData={initData}
                handleData={setData}
                handleTransitionData={setTransitionData}
                handleCurrentSlideData={setCurrentSlideData}
                sliderData={sliderData}
              />
            </div>
          </div>
        </div>
      </AnimatePresence>
    </section>
  );
}

const initData: SliderItem = sliderData[0];
