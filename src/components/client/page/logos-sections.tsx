'use client';
import { Image } from '@nextui-org/react';
export default function LogosSections() {
  return (
    <section>
      <div className="mx-auto w-full max-w-7xl px-5 py-16 md:px-10 md:py-24 lg:py-32">
        <h5 className="mb-6 text-center text-xl font-bold md:mb-12">
          NUESTROS CLIENTES:
        </h5>
        <h6 className="mb-2 text-center text-md font-bold md:mb-12">
          Ingeniería corporativa para conseguir proyectos con Alma.
        </h6>

        <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 items-center justify-center gap-8 rounded-md bg-main-100 p-16 px-8 py-12 md:gap-16">
          <div className="flex items-center justify-center">
            <Image
              src="/logos/1.png"
              alt="logo"
              className="max-w-full items-center justify-center sm:max-w-[80%]"
            />
          </div>
          <div className="flex items-center justify-center">
            <Image
              src="/logos/2.png"
              alt="logo"
              className="max-w-full items-center justify-center sm:max-w-[80%]"
            />
          </div>
          <div className="flex items-center justify-center">
            <Image
              src="/logos/3.png"
              alt="logo"
              className="max-w-full items-center justify-center sm:max-w-[80%]"
            />
          </div>
          <div className="flex items-center justify-center">
            <Image
              src="/logos/4.png"
              alt="logo"
              className="max-w-full items-center justify-center sm:max-w-[80%]"
            />
          </div>
          <div className="flex items-center justify-center">
            <Image
              src="/logos/5.png"
              alt="logo"
              className="max-w-full items-center justify-center sm:max-w-[80%]"
            />
          </div>
          <div className="flex items-center justify-center">
            <Image
              src="/logos/6.png"
              alt="logo"
              className="max-w-full items-center justify-center sm:max-w-[80%]"
            />
          </div>
          <div className="flex items-center justify-center">
            <Image
              src="/logos/7.png"
              alt="logo"
              className="max-w-full items-center justify-center sm:max-w-[80%]"
            />
          </div>
          <div className="flex items-center justify-center">
            <Image
              src="/logos/10.png"
              alt="logo"
              className="max-w-full items-center justify-center sm:max-w-[80%]"
            />
          </div>
          <div className="flex items-center justify-center">
            <Image
              src="/logos/9.png"
              alt="logo"
              className="max-w-full items-center justify-center sm:max-w-[80%]"
            />
          </div>
        </div>
      </div>
    </section>
  );
}
