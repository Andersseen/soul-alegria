"use client";
import { CheckIcon } from "@/components/icons/check";
import { PUBLICITIES as publicities } from "@/data/publicities";
import {
  Card,
  CardHeader,
  CardBody,
  Divider,
  Listbox,
  ListboxItem,
} from "@nextui-org/react";
export const Publicity = () => {
  return (
    <div className="grid grid-cols-12 gap-4 px-8 mt-6">
      {publicities.map((item) => (
        <Card
          key={item.id}
          className="col-span-12 md:col-span-6 lg:col-span-4 bg-transparent"
        >
          <CardHeader className="flex gap-3">
            <CheckIcon />

            <h5 className="text-md">{item.title}</h5>
          </CardHeader>
          <Divider />
          <CardBody>
            <Listbox variant="flat">
              {item.names.map((item, index) => (
                <ListboxItem key={index}>
                  <p className="text-xs text-semibold text-center">{item}</p>
                </ListboxItem>
              ))}
            </Listbox>
          </CardBody>
        </Card>
      ))}
    </div>
  );
};
