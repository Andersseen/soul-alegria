"use client";
import { motion, useMotionTemplate, useMotionValue } from "framer-motion";
import { Listbox, ListboxItem } from "@nextui-org/react";
import { Tabs, Tab, Card, CardBody } from "@nextui-org/react";
import { CheckIcon } from "@/components/icons/check";

export const Trading = ({ head, body }: { head: any; body: any }) => {
  const mouseX = useMotionValue(0);
  const mouseY = useMotionValue(0);

  function handleMouseMove({ currentTarget, clientX, clientY }) {
    const { left, top } = currentTarget.getBoundingClientRect();

    mouseX.set(clientX - left);
    mouseY.set(clientY - top);
  }

  return (
    <div
      className="group relative px-8 py-16 shadow-2xl"
      onMouseMove={handleMouseMove}
    >
      <motion.div
        className="pointer-events-none absolute -inset-px rounded-xl opacity-0 transition duration-300 group-hover:opacity-100"
        style={{
          background: useMotionTemplate`
            radial-gradient(
              650px circle at ${mouseX}px ${mouseY}px,
              rgba(248, 113, 113, 0.1),
              transparent 50%
            )
          `,
        }}
      />
      <section className="relative isolate px-6 pt-14 lg:px-8">
        <div className="text-left">
          <motion.h3
            className="text-2xl font-bold tracking-tight"
            initial={{ opacity: 0, y: "-10%" }}
            whileInView={{ opacity: 1, y: 0 }}
            viewport={{ once: true }}
            transition={{ duration: 0.7 }}
          >
            {head}
          </motion.h3>

          <Listbox variant="flat">
            {body.map((item, index) => {
              return (
                <ListboxItem key={index} startContent={<CheckIcon />}>
                  <p className="text-lg text-semibold">{item}</p>
                </ListboxItem>
              );
            })}
          </Listbox>
        </div>
      </section>
    </div>
  );
};

export const TradingSection = () => {
  const tabs = [
    {
      id: "PRODUCTO",
      label: "GESTIÓN DE PRODUCTO",
      content: `Ofrecemos nuevos mercados y países a su producto. Compramos también partidas de producto procedentes de sobreproducción, promoción, baja rotación, liquidación, etc. y los colocamos en mercados identificados tanto al nivel nacional como internacional.`,
    },
    {
      id: "EXPORTACIoN",
      label: "EXPORTACIÓN",
      content: `Ofrecemos mercados alternativos/identificados para su producto, respectando 
sus restricciones de venta, tanto al nivel Nacional (Colectivos, Redes, etc.) como 
Internacional (País/Mercados identificados).`,
    },
    {
      id: "IMPORTACIoN",
      label: "IMPORTACIÓN",
      content: `Tenemos acuerdos de exclusividad con productos internacionales que integramos en el 
mercado nacional. Nuestra equipo de Trade llega a cabo acuerdos B2B para su 
comercialización.
`,
    },
  ];
  return (
    <div className="flex w-full flex-col mt-8">
      <Tabs
        items={tabs}
        radius="full"
        color="secondary"
        variant="bordered"
        className="flex justify-center bg-inherit"
      >
        {(item) => (
          <Tab key={item.id} title={item.label}>
            <Card className="bg-inherit">
              <CardBody>{item.content}</CardBody>
            </Card>
          </Tab>
        )}
      </Tabs>
    </div>
  );
};
