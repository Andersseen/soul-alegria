import { CheckedIcon } from "@/components/icons/check";

export const WhyTop = () => {
  const features = [
    {
      id: "1a2",
      imgSrc: "/icons/checked.png",
      title: "MÁS DE 500 CAMPAÑAS REALIZADAS EN LOS ÚLTIMOS DIEZ AÑOS",

      description: `¿ Busca optimizar su inversión publicitaria con su agencia de medios? 
      ¿aumentar su mancha publicitaria (on/off) sin aumentar su presupuesto? 
      Gracias al Smart Trading, Barter o Media Credit podrás conseguirlo en 
      colaboración con su agencia de medios si tiene.`,
    },
    {
      id: "2a3",
      imgSrc: "/icons/checked.png",
      title: "HABLEMOS Y VEMOS OPCIONES",

      description: `¿Desea vender su producto en nuevos canales/países? ¿Desea vender, al 
      mejor precio, sus productos “slow mover”, depreciados o no productivos? 
      Con nuestro depto de Trade y partners internacionales le damos 
      alternativas.`,
    },
    {
      id: "3r4",
      imgSrc: "/icons/checked.png",
      title: "OFRECEMOS SOLUCIONES AD-HOC",

      description: `¿Existen nuevas herramientas para mejorar su productividad? 
      En los últimos 20 años hemos introducido nuevas herramientas 
      internacionales para mejorar los resultados de empresas, tanto en 
      optimizar inversiones publicitarias, aumentar ventas, mejorando 
      provisiones y comprar mejor.`,
    },
    {
      id: "4v5",
      imgSrc: "/icons/checked.png",
      title: "LO HEMOS CONSEGUIDO",

      description: `¿Crees en valores sociales? ¿Es posible que desde una empresa se puede 
      ayudar a integrar en la sociedad a personas con dificultad auditiva-hipoacúsicos?.`,
    },
  ];

  return (
    <div className="container my-6 mx-auto md:px-6">
      <h2 className="mb-6 text-3xl font-bold text-center">
        ¿PORQUE Soul&Alegría?
      </h2>
      <section className="mb-32">
        <div className="flex flex-wrap items-center">
          <div className="mb-6 w-full shrink-0 grow-0 basis-auto px-3 lg:mb-0 lg:w-4/12">
            <h3 className="mb-6 text-2xl font-semibold self-center">
              VALORES :
            </h3>
            <div className="flex flex-col items-center">
              <p>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="128"
                  height="128"
                  viewBox="0 0 24 24"
                >
                  <g
                    style={{
                      stroke: "var(--foreground-color)",
                    }}
                    fill="none"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                  >
                    <path d="M3 12a9 9 0 1 0 18 0a9 9 0 0 0-18 0m.6-3h16.8M3.6 15h16.8" />
                    <path d="M11.5 3a17 17 0 0 0 0 18m1-18a17 17 0 0 1 0 18" />
                  </g>
                </svg>
              </p>
              <h4 className="text-xl font-semibold">
                MEJORA CONTINUA CON INOVACION
              </h4>
              <p>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="128"
                  height="128"
                  viewBox="0 0 32 32"
                >
                  <path
                    style={{
                      fill: `var(--foreground-color)`,
                    }}
                    d="M13 2a6.007 6.007 0 0 0-6 6h2a4 4 0 0 1 8 0h2a6.007 6.007 0 0 0-6-6Z"
                  />
                  <path
                    style={{
                      fill: `var(--foreground-color)`,
                    }}
                    d="M21 30h-4.44a4 4 0 0 1-2.708-1.057l-9.2-8.466a2.002 2.002 0 0 1 .118-3.055a2.074 2.074 0 0 1 2.658.173L11 20.857V8a2 2 0 0 1 4 0v7a2 2 0 0 1 4 0v1a2 2 0 0 1 4 0v1a2 2 0 0 1 4 0v7a6 6 0 0 1-6 6Z"
                  />
                </svg>
              </p>
              <h4 className="text-xl font-semibold">
                CONOCICMIENTO INTERNACIONAL
              </h4>
              <p>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="128"
                  height="128"
                  viewBox="0 0 24 24"
                >
                  <path
                    style={{
                      fill: `var(--foreground-color)`,
                    }}
                    d="M12 23v-2h7v-1h-4v-8h4v-1q0-2.9-2.05-4.95T12 4Q9.1 4 7.05 6.05T5 11v1h4v8H5q-.825 0-1.413-.588T3 18v-7q0-1.85.713-3.488T5.65 4.65q1.225-1.225 2.863-1.938T12 2q1.85 0 3.488.713T18.35 4.65q1.225 1.225 1.938 2.863T21 11v10q0 .825-.588 1.413T19 23h-7Z"
                  />
                </svg>
              </p>
              <h4 className="text-xl font-semibold">
                PESONALIZACIÓN A CADA CLIENTE
              </h4>
            </div>
          </div>

          <div className="mb-md-0 mb-6 w-full shrink-0 grow-0 basis-auto px-3 lg:w-8/12">
            <div className="flex flex-wrap">
              {features.map((feature) => (
                <div
                  key={feature.id}
                  className="mb-12 w-full shrink-0 grow-0 basis-auto lg:px-3"
                >
                  <div className="flex">
                    <div className="mt-0.5 shrink-0 self-center">
                      <CheckedIcon />
                    </div>
                    <div className="ml-2 grow">
                      <p className="mb-3 font-bold">{feature.description}</p>
                      <p className="text-base text-main-400">{feature.title}</p>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};
