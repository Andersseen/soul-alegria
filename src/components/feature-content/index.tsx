'use client';
import { Image } from '@nextui-org/react';
import { Button } from '@nextui-org/react';
import { FeatureContentData } from '../../utils';

export const FeatureContent = ({
  featureContentData,
}: {
  featureContentData: FeatureContentData;
}) => {
  return (
    <section className="overflow-hidden sm:grid sm:grid-cols-2">
      <div className="p-8 md:p-12 lg:px-16 lg:py-24">
        <div className="mx-auto max-w-xl text-center ltr:sm:text-left rtl:sm:text-right">
          <h2 className="text-2xl font-bold md:text-3xl">
            {featureContentData.title}
          </h2>
          <div className="text-left">
            {featureContentData.paragraphs.map((row: string, index: number) => (
              <p className="hidden md:mt-4 md:block" key={`${row}-${index}`}>
                {row}
              </p>
            ))}
          </div>

          <div className="mt-4 md:mt-8">
            <Button color={featureContentData.button.color}>
              {featureContentData.button.name}
            </Button>
          </div>
        </div>
      </div>

      <Image
        alt={featureContentData.image.altImg}
        src={featureContentData.image.srcImg}
      />
    </section>
  );
};
