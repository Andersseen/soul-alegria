'use client';
import React from 'react';
import Link from 'next/link';
import { motion } from 'framer-motion';
import { NavigationLink } from '../../utils';
import { FacebookIcon, InstagramIcon } from 'lucide-react';

interface FooterProps {
  footerHeight: (height: number) => void;
  title: string;
  isVisible: boolean;
  pages: NavigationLink[];
}

export const FooterSection: React.FC<FooterProps> = ({
  footerHeight,
  title,
  isVisible,
  pages,
}) => {
  const iconProps = {
    fill: 'var(--foreground-color)',
    width: '32px',
    height: '32px',
  };

  const footerRef = React.useRef<HTMLDivElement | null>(null);

  React.useEffect(() => {
    if (footerRef.current) {
      const height = footerRef.current.offsetHeight;
      footerHeight(height);
    }
  }, [footerHeight]);

  const currentYear = new Date().getFullYear();

  return (
    <footer
      ref={footerRef}
      className="fixed bottom-0 left-0 w-full text-main-900 dark:text-main-100 text-center p-4 h-100"
    >
      <motion.div
        animate={isVisible ? { opacity: 1, y: 0 } : { opacity: 0, y: '100%' }}
        transition={{ duration: 1, ease: 'easeInOut' }}
        className="mx-auto max-w-screen-xl"
      >
        <div className="md:flex md:justify-between">
          <div className="mb-6 md:mb-0">
            <Link href="#" className="flex items-center">
              <span className="self-center text-2xl font-semibold whitespace-nowrap">
                {title}
              </span>
            </Link>
          </div>
          <div className="grid grid-cols-2 gap-8 sm:gap-6 sm:grid-cols-3">
            <div>
              <h2 className="mb-6 text-sm font-semibold text-main-900 uppercase dark:text-main-100">
                Recursos
              </h2>
              <ul>
                {pages.map((value: NavigationLink, index: number) => (
                  <li className="mb-2" key={`${value.label}-${index}`}>
                    <Link
                      href={value.route}
                      className="text-xs hover:underline"
                    >
                      {value.label}
                    </Link>
                  </li>
                ))}
              </ul>
            </div>
            <div>
              <h2 className="mb-6 text-sm font-semibold text-main-900 uppercase dark:text-main-100">
                Síguenos en
              </h2>
              <ul className="text-main-600 dark:text-main-400">
                <li className="mb-4">
                  <Link href="/" className="hover:underline ">
                    Facebook
                  </Link>
                </li>
                <li>
                  <a href="/" className="hover:underline">
                    Instagram
                  </a>
                </li>
              </ul>
            </div>
            <div>
              <h2 className="mb-6 text-sm font-semibold text-main-900 uppercase dark:text-main-100">
                Legal
              </h2>
              <ul className="text-main-600 dark:text-main-400">
                <li className="mb-4">
                  <Link href="#" className="hover:underline">
                    Privacy Policy
                  </Link>
                </li>
                <li>
                  <Link href="#" className="hover:underline">
                    Terms &amp; Conditions
                  </Link>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <hr className="my-6 border-main-200 sm:mx-auto dark:border-main-700 lg:my-8" />
        <div className="sm:flex sm:items-center sm:justify-between">
          <span className="text-sm text-main-500 sm:text-center dark:text-main-400">
            Copyright © {currentYear}
          </span>
          <div className="flex mt-4 space-x-6 sm:justify-center sm:mt-0">
            <Link
              href="#"
              className="text-main-500 hover:text-main-900 dark:hover:text-white"
            >
              <FacebookIcon  />
            </Link>
            <Link
              href="#"
              className="text-main-500 hover:text-main-900 dark:hover:text-white"
            >
              <InstagramIcon  />
            </Link>
          </div>
        </div>
      </motion.div>
    </footer>
  );
};
