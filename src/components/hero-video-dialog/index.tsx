"use client";
import { useState, useEffect } from "react";
import { AnimatePresence, motion } from "framer-motion";
import { Play, XIcon } from "lucide-react";
import { cn } from "@/lib/utils";

type AnimationStyle =
  | "from-bottom"
  | "from-center"
  | "from-top"
  | "from-left"
  | "from-right"
  | "fade"
  | "top-in-bottom-out"
  | "left-in-right-out";

interface HeroVideoProps {
  animationStyle?: AnimationStyle;
  videoSrc: string;
  thumbnailAlt?: string;
  className?: string;
}

const animationVariants = {
  "from-bottom": {
    initial: { y: "100%", opacity: 0 },
    animate: { y: 0, opacity: 1 },
    exit: { y: "100%", opacity: 0 },
  },
  "from-center": {
    initial: { scale: 0.5, opacity: 0 },
    animate: { scale: 1, opacity: 1 },
    exit: { scale: 0.5, opacity: 0 },
  },
  "from-top": {
    initial: { y: "-100%", opacity: 0 },
    animate: { y: 0, opacity: 1 },
    exit: { y: "-100%", opacity: 0 },
  },
  "from-left": {
    initial: { x: "-100%", opacity: 0 },
    animate: { x: 0, opacity: 1 },
    exit: { x: "-100%", opacity: 0 },
  },
  "from-right": {
    initial: { x: "100%", opacity: 0 },
    animate: { x: 0, opacity: 1 },
    exit: { x: "100%", opacity: 0 },
  },
  fade: {
    initial: { opacity: 0 },
    animate: { opacity: 1 },
    exit: { opacity: 0 },
  },
  "top-in-bottom-out": {
    initial: { y: "-100%", opacity: 0 },
    animate: { y: 0, opacity: 1 },
    exit: { y: "100%", opacity: 0 },
  },
  "left-in-right-out": {
    initial: { x: "-100%", opacity: 0 },
    animate: { x: 0, opacity: 1 },
    exit: { x: "100%", opacity: 0 },
  },
};

export function HeroVideoDialog({
  animationStyle = "from-center",
  videoSrc,
  thumbnailAlt = "Video thumbnail",
  className,
}: HeroVideoProps) {
  const [isVideoOpen, setIsVideoOpen] = useState(false);
  const [isVideoHidden, setIsVideoHidden] = useState<boolean>(() => {
    if (typeof window !== "undefined") {
      return localStorage.getItem("isVideoHidden") === "true";
    }
    return false;
  });

  useEffect(() => {
    const hidden = localStorage.getItem("isVideoHidden");
    if (hidden === "true") {
      setIsVideoHidden(true);
    }
  }, []);

  useEffect(() => {
    localStorage.setItem("isVideoHidden", String(isVideoHidden));
  }, [isVideoHidden]);

  const selectedAnimation = animationVariants[animationStyle];

  return (
    <div
      className={cn(
        isVideoHidden
          ? "z-30 fixed bottom-4 right-4"
          : "z-30 fixed w-full h-screen",
        className
      )}
    >
      {!isVideoHidden && (
        <div
          className="relative cursor-pointer group h-screen flex flex-col justify-center"
          onClick={() => setIsVideoOpen(true)}
        >
          <div className="absolute inset-0 flex items-center justify-center group-hover:scale-100 scale-[0.9] transition-all duration-200 ease-out rounded-2xl">
            <div className="bg-main-300/10 flex items-center justify-center rounded-full backdrop-blur-md size-28">
              <div
                className={`flex items-center justify-center bg-gradient-to-b from-main-300/30 to-main-500 shadow-md rounded-full size-20 transition-all ease-out duration-200 relative group-hover:scale-[7] scale-[5]`}
              >
                <Play
                  className="size-8 text-white fill-white group-hover:scale-105 scale-100 transition-transform duration-200 ease-out"
                  style={{
                    filter:
                      "drop-shadow(0 4px 3px rgb(0 0 0 / 0.07)) drop-shadow(0 2px 2px rgb(0 0 0 / 0.06))",
                  }}
                />
              </div>
            </div>
          </div>
        </div>
      )}
      {isVideoHidden && (
        <div
          className="cursor-pointer group"
          onClick={() => setIsVideoOpen(true)}
        >
          <div
            className={`flex items-center justify-center bg-gradient-to-b from-main-300/30 to-main-500 shadow-md rounded-full size-20 transition-all ease-out duration-200 relative group-hover:scale-[1.5] scale-[1.2]`}
          >
            <Play
              className="size-6 text-white fill-white group-hover:scale-105 scale-100 transition-transform duration-200 ease-out"
              style={{
                filter:
                  "drop-shadow(0 4px 3px rgb(0 0 0 / 0.07)) drop-shadow(0 2px 2px rgb(0 0 0 / 0.06))",
              }}
            />
          </div>
        </div>
      )}
      <AnimatePresence>
        {isVideoOpen && (
          <motion.div
            initial={{ opacity: 0 }}
            animate={{ opacity: 1 }}
            onClick={() => setIsVideoOpen(false)}
            exit={{ opacity: 0 }}
            className="fixed top-10 inset-0 z-50 flex items-center justify-center bg-black/50 backdrop-blur-md"
          >
            <motion.div
              {...selectedAnimation}
              transition={{ type: "spring", damping: 30, stiffness: 300 }}
              className="relative w-full max-w-4xl aspect-video mx-4 md:mx-0"
            >
              <button
                className="absolute -top-16 right-0 text-white text-xl bg-neutral-900/50 ring-1 backdrop-blur-md rounded-full p-2 dark:bg-neutral-100/50 dark:text-black"
                onClick={() => {
                  setIsVideoHidden(true);
                  setIsVideoOpen(false);
                }}
              >
                <XIcon className="size-5" />
              </button>
              <div className="size-full border-2 border-white rounded-2xl overflow-hidden isolate z-[1] relative w-full h-full">
                <iframe
                  src={videoSrc}
                  className="size-full rounded-2xl w-full h-full"
                  allowFullScreen
                  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                ></iframe>
              </div>
            </motion.div>
          </motion.div>
        )}
      </AnimatePresence>
    </div>
  );
}
