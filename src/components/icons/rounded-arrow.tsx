'use client';
import { motion } from 'framer-motion';

const rotateVariants = {
  rotated: { rotate: 180 },
  default: { rotate: 0 },
};

export const ArrowLeftToRightRounded = ({
  isHovered,
}: {
  isHovered: boolean;
}) => {
  return (
    <motion.svg
      xmlns="http://www.w3.org/2000/svg"
      width="2em"
      height="2em"
      viewBox="0 0 24 24"
      animate={isHovered ? 'rotated' : 'default'}
      variants={rotateVariants}
      transition={{ duration: 0.2, ease: 'easeInOut' }}
    >
      <path
        fill="currentColor"
        d="m13.15 16.15l-3.625-3.625q-.125-.125-.175-.25T9.3 12q0-.15.05-.275t.175-.25L13.15 7.85q.075-.075.163-.112T13.5 7.7q.2 0 .35.138T14 8.2v7.6q0 .225-.15.363t-.35.137q-.05 0-.35-.15"
      />
    </motion.svg>
  );
};

export function ArrowDownToUpRounded({ isHovered }: { isHovered: boolean }) {
  return (
    <motion.svg
      xmlns="http://www.w3.org/2000/svg"
      width="2em"
      height="2em"
      viewBox="0 0 24 24"
      animate={isHovered ? 'rotated' : 'default'}
      variants={rotateVariants}
      transition={{ duration: 0.2, ease: 'easeInOut' }}
    >
      <path
        fill="currentColor"
        d="M11.475 14.475L7.85 10.85q-.075-.075-.112-.162T7.7 10.5q0-.2.138-.35T8.2 10h7.6q.225 0 .363.15t.137.35q0 .05-.15.35l-3.625 3.625q-.125.125-.25.175T12 14.7q-.15 0-.275-.05t-.25-.175"
      />
    </motion.svg>
  );
}

export function ArrowUpToDownRounded({ isHovered }: { isHovered: boolean }) {
  return (
    <motion.svg
      xmlns="http://www.w3.org/2000/svg"
      width="2em"
      height="2em"
      viewBox="0 0 24 24"
      animate={isHovered ? 'rotated' : 'default'}
      variants={rotateVariants}
      transition={{ duration: 0.2, ease: 'easeInOut' }}
    >
      <path
        fill="currentColor"
        d="M8.2 14q-.225 0-.362-.15T7.7 13.5q0-.05.15-.35l3.625-3.625q.125-.125.25-.175T12 9.3q.15 0 .275.05t.25.175l3.625 3.625q.075.075.113.163t.037.187q0 .2-.137.35T15.8 14z"
      />
    </motion.svg>
  );
}
