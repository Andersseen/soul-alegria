'use client';
import { motion, useMotionTemplate, useMotionValue } from 'framer-motion';

export const LightWrapper = ({ children }: { children: React.ReactNode }) => {
  const mouseX = useMotionValue(0);
  const mouseY = useMotionValue(0);

  // const handleMouseMove = (event: MouseEvent) => {
  const handleMouseMove = (event: any) => {
    if (event.currentTarget) {
      const { left, top } = event.currentTarget.getBoundingClientRect();
      mouseX.set(event.clientX - left);
      mouseY.set(event.clientY - top);
    }
  };

  return (
    <section
      className="group relative px-0 sm:px-8 py-16 shadow-2xl min-h-screen min-w-screen"
      onMouseMove={handleMouseMove}
    >
      <motion.div
        className="pointer-events-none absolute -inset-px rounded-xl opacity-0 transition duration-300 group-hover:opacity-100"
        style={{
          background: useMotionTemplate`
            radial-gradient(
              650px circle at ${mouseX}px ${mouseY}px,
              var(--light-color),
              transparent 50%
            )
          `,
        }}
      />
      <div className="relative isolate px-0 sm:px-6 pt-14 lg:px-8">
        {children}
      </div>
    </section>
  );
};
