'use client';
import { Image } from '@nextui-org/react';

const srcImg = '/logo.png';

export const Logo = () => {
  return <Image width={200} height={200} alt="Logo" src={srcImg} />;
};
