"use client";

import { useTheme } from "next-themes";
import { HeaderSection } from "../header-section";
import { TextContent } from "../text-content";
import { HeaderData, TextContentData } from "@/utils";

export const PageContent = ({
  headerData,
  textContentData,
}: {
  headerData: HeaderData;
  textContentData: TextContentData;
}) => {
  const { resolvedTheme } = useTheme();

  return (
    <>
      <HeaderSection headerData={headerData} resolvedTheme={resolvedTheme} />
      <TextContent textContentData={textContentData} />
    </>
  );
};

export const PageWithHeader = ({
  headerData,
  children,
}: {
  headerData: HeaderData;
  children: React.ReactNode;
}) => {
  const { resolvedTheme } = useTheme();

  return (
    <>
      <HeaderSection headerData={headerData} resolvedTheme={resolvedTheme} />
      {children}
    </>
  );
};
