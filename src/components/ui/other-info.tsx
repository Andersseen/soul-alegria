import React from 'react';
import { motion } from 'framer-motion';

type Props = {
  data: any;
};
const item = {
  hidden: {
    y: '100%',
    transition: { ease: [0.455, 0.03, 0.515, 0.955], duration: 0.85 },
  },
  visible: {
    y: 0,
    transition: { ease: [0.455, 0.03, 0.515, 0.955], duration: 0.75 },
  },
};

function OtherInfo({ data }: Props) {
  return (
    <>
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        exit={{ opacity: 0 }}
        className=" flex flex-col"
      >
        {data?.description.map((item, index) => (
          <BubbleText key={`description-${index}`} text={item} />
        ))}
      </motion.div>
      <motion.div
        initial="hidden"
        animate={'visible'}
        className=" flex flex-col"
      >
        <AnimatedText
          className=" my-1 text-4xl font-semibold md:my-3 md:text-8xl md:leading-[100px]"
          data={data?.title}
        />
        <AnimatedText className="text-sm" data={data?.subtitle} />
      </motion.div>
    </>
  );
}

export default OtherInfo;

const AnimatedText = ({
  data,
  className,
}: {
  data?: string;
  className?: string;
}) => {
  return (
    <span
      style={{
        overflow: 'hidden',
        display: 'inline-block',
      }}
    >
      <motion.p className={` ${className}`} variants={item} key={data}>
        {data}
      </motion.p>
    </span>
  );
};

const BubbleText = ({ text }: { text: string }) => {
  const highlightText = (text: string) => {
    const wordsToHighlight = [
      'MAXIMISAR EL VALOR DE TUS RECURSOS Y OPTIMIZAR TU PRESENCIA EN MEDIOS',
      'ES NUESTRO ADN',
      'OFRECEMOS HERRAMIENTAS INTERNACIONALES INNOVADORAS Y EFECTIVAS',
      'UN ENFOQUE UNICO',
      'Y SI BUSCAS ALTERNATIVAS Y DIFERENCIACION EMPRESARIAL SOMOS TU SOLUCION',
      'SERVICIOS DE MARKETING PERSONALIZADOS OFFLINE/ONLINE',
      'ROI ES NUESTRO ADN',
      'ROI A\\+',
      'ACUERDOS ESPECIALES CON MEDIOS DE COMUNICACIÓN',
      'COMPRAMOS ',
      'A UN PRECIO 2 A 3 VECES SUPERIOR QUE LOS LIQUIDADORES DE STOCK',
      'NUEVOS MERCADOS Y PAISES',
      'IMPORTACION EXITOSA',
      '\\(DISTRIBUIDOR, B2B, B2C\\)',
    ];
    const regex = new RegExp(`(${wordsToHighlight.join('|')})`, 'gi');

    return text.split(regex).map((part, index) =>
      regex.test(part) ? (
        <span key={index} className="font-bold text-main-400">
          {part}
        </span>
      ) : (
        part
      )
    );
  };

  return (
    <h2 className="text-left md:mb-4 text-sm font-thin text-neutral-100 [text-shadow:_1px_1px_0_rgb(0_0_0_/_40%)]">
      {highlightText(text)}
    </h2>
  );
};
