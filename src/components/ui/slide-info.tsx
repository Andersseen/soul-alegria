"use client";
import React from "react";
import { motion } from "framer-motion";
import { SliderItem, CurrentSlideData } from "@/data/slides";
import Link from "next/link";
import { Button } from "@nextui-org/react";
import OtherInfo from "./other-info";

type Props = {
  transitionData: SliderItem;
  currentSlideData: CurrentSlideData;
};

function SlideInfo({ transitionData, currentSlideData }: Props) {
  return (
    <>
      <motion.span layout className=" mb-2 h-1 w-5 rounded-full bg-white" />
      <OtherInfo
        data={transitionData ? transitionData : currentSlideData.data}
      />
      <motion.div layout className=" mt-5 flex items-center gap-3">
        <Button color="secondary" radius="full">
          <Link href={transitionData.route}>Ver el servicio</Link>
        </Button>
      </motion.div>
    </>
  );
}

export default SlideInfo;
