"use client";
import React from "react";
import { SliderItem } from "@/data/slides";
import SliderCard from "./slider-card";

type Props = {
  data: SliderItem[];
};

function Slides({ data }: Props) {
  return (
    <div className=" flex w-full gap-6">
      {data.map((data) => {
        return <SliderCard key={data.img} data={data} />;
      })}
    </div>
  );
}

export default Slides;
