export const POSTS = [
  {
    id: 1,
    title: 'Acuerdo histórico',
    href: 'blog/1',
    description:
      'Un pequeño paso de las instituciones, un gran paso para la comunidad. Querida familia de personas con hipoacusia. Queridas personas con conciencia y empatía. Querida sociedad. Hoy volvemos, tras una...',
    date: 'Mar 16, 2020',
    datetime: '2020-03-16',
    category: { title: 'Soul&Alegría', href: '#' },
    imageUrl:
      'https://www.soulalegria.com/images/blog/Acuerdo-Thyssen-Bankinter.jpg',
  },
  {
    id: 2,
    title: 'Normalizar la hipoacusia',
    href: '#',
    description:
      'La normalización de las personas con problemas auditivos está recorriendo un largo camino. A través de asociaciones, instituciones, empresas del sector, pero sobre todo la verbalización y proyección...',
    date: 'Mar 16, 2020',
    datetime: '2020-03-16',
    category: { title: 'Soul&Alegría', href: '#' },
    imageUrl: 'https://www.soulalegria.com/images/blog/Estela-Lorenzo.png',
  },
  {
    id: 3,
    title: 'Normalizar la hipoacusia',
    href: '#',
    description:
      'La normalización de las personas con problemas auditivos está recorriendo un largo camino. A través de asociaciones, instituciones, empresas del sector, pero sobre todo la verbalización y proyección...',
    date: 'Mar 16, 2020',
    datetime: '2020-03-16',
    category: { title: 'Soul&Alegría', href: '#' },
    imageUrl:
      'https://soulalegria.com/images/blog/un-dia-como-tecnico-de-sonido.jpg',
  },
  {
    id: 4,
    title: 'Normalizar la hipoacusia',
    href: '#',
    description:
      'La normalización de las personas con problemas auditivos está recorriendo un largo camino. A través de asociaciones, instituciones, empresas del sector, pero sobre todo la verbalización y proyección...',
    date: 'Mar 16, 2020',
    datetime: '2020-03-16',
    category: { title: 'Soul&Alegría', href: '#' },
    imageUrl: 'https://soulalegria.com/images/blog/Oir-a-diario.jpg',
  },
  {
    id: 5,
    title: 'Normalizar la hipoacusia',
    href: '#',
    description:
      'La normalización de las personas con problemas auditivos está recorriendo un largo camino. A través de asociaciones, instituciones, empresas del sector, pero sobre todo la verbalización y proyección...',
    date: 'Mar 16, 2020',
    datetime: '2020-03-16',
    category: { title: 'Soul&Alegría', href: '#' },
    imageUrl: 'https://soulalegria.com/images/blog/stress-y-audicin3.jpg',
  },
  {
    id: 6,
    title: 'Normalizar la hipoacusia',
    href: '#',
    description:
      'La normalización de las personas con problemas auditivos está recorriendo un largo camino. A través de asociaciones, instituciones, empresas del sector, pero sobre todo la verbalización y proyección...',
    date: 'Mar 16, 2020',
    datetime: '2020-03-16',
    category: { title: 'Soul&Alegría', href: '#' },
    imageUrl: 'https://soulalegria.com/images/CARTEL_25F.png',
  },
  {
    id: 7,
    title: 'Normalizar la hipoacusia',
    href: '#',
    description:
      'La normalización de las personas con problemas auditivos está recorriendo un largo camino. A través de asociaciones, instituciones, empresas del sector, pero sobre todo la verbalización y proyección...',
    date: 'Mar 16, 2020',
    datetime: '2020-03-16',
    category: { title: 'Soul&Alegría', href: '#' },
    imageUrl: 'https://soulalegria.com/images/blog/senales-entorno.png',
  },
  {
    id: 8,
    title: 'Normalizar la hipoacusia',
    href: '#',
    description:
      'La normalización de las personas con problemas auditivos está recorriendo un largo camino. A través de asociaciones, instituciones, empresas del sector, pero sobre todo la verbalización y proyección...',
    date: 'Mar 16, 2020',
    datetime: '2020-03-16',
    category: { title: 'Soul&Alegría', href: '#' },
    imageUrl: 'https://soulalegria.com/images/blog/Welcome-2021.jpg',
  },
  {
    id: 9,
    title: 'Normalizar la hipoacusia',
    href: '#',
    description:
      'La normalización de las personas con problemas auditivos está recorriendo un largo camino. A través de asociaciones, instituciones, empresas del sector, pero sobre todo la verbalización y proyección...',
    date: 'Mar 16, 2020',
    datetime: '2020-03-16',
    category: { title: 'Soul&Alegría', href: '#' },
    imageUrl: 'https://www.soulalegria.com/images/blog/Estela-Lorenzo.png',
  },
  {
    id: 10,
    title: 'Normalizar la hipoacusia',
    href: '#',
    description:
      'La normalización de las personas con problemas auditivos está recorriendo un largo camino. A través de asociaciones, instituciones, empresas del sector, pero sobre todo la verbalización y proyección...',
    date: 'Mar 16, 2020',
    datetime: '2020-03-16',
    category: { title: 'Soul&Alegría', href: '#' },
    imageUrl: 'https://www.soulalegria.com/images/blog/Estela-Lorenzo.png',
  },
  {
    id: 11,
    title: 'Normalizar la hipoacusia',
    href: '#',
    description:
      'La normalización de las personas con problemas auditivos está recorriendo un largo camino. A través de asociaciones, instituciones, empresas del sector, pero sobre todo la verbalización y proyección...',
    date: 'Mar 16, 2020',
    datetime: '2020-03-16',
    category: { title: 'Soul&Alegría', href: '#' },
    imageUrl: 'https://www.soulalegria.com/images/blog/Estela-Lorenzo.png',
  },
  {
    id: 12,
    title: 'Normalizar la hipoacusia',
    href: '#',
    description:
      'La normalización de las personas con problemas auditivos está recorriendo un largo camino. A través de asociaciones, instituciones, empresas del sector, pero sobre todo la verbalización y proyección...',
    date: 'Mar 16, 2020',
    datetime: '2020-03-16',
    category: { title: 'Soul&Alegría', href: '#' },
    imageUrl: 'https://www.soulalegria.com/images/blog/Estela-Lorenzo.png',
  },
  {
    id: 13,
    title: 'Normalizar la hipoacusia',
    href: '#',
    description:
      'La normalización de las personas con problemas auditivos está recorriendo un largo camino. A través de asociaciones, instituciones, empresas del sector, pero sobre todo la verbalización y proyección...',
    date: 'Mar 16, 2020',
    datetime: '2020-03-16',
    category: { title: 'Soul&Alegría', href: '#' },
    imageUrl: 'https://www.soulalegria.com/images/blog/Estela-Lorenzo.png',
  },
  {
    id: 14,
    title: 'Normalizar la hipoacusia',
    href: '#',
    description:
      'La normalización de las personas con problemas auditivos está recorriendo un largo camino. A través de asociaciones, instituciones, empresas del sector, pero sobre todo la verbalización y proyección...',
    date: 'Mar 16, 2020',
    datetime: '2020-03-16',
    category: { title: 'Soul&Alegría', href: '#' },
    imageUrl:
      'https://soulalegria.com/images/blog/un-dia-como-tecnico-de-sonido.jpg',
  },
  {
    id: 15,
    title: 'Normalizar la hipoacusia',
    href: '#',
    description:
      'La normalización de las personas con problemas auditivos está recorriendo un largo camino. A través de asociaciones, instituciones, empresas del sector, pero sobre todo la verbalización y proyección...',
    date: 'Mar 16, 2020',
    datetime: '2020-03-16',
    category: { title: 'Soul&Alegría', href: '#' },
    imageUrl:
      'https://soulalegria.com/images/blog/un-dia-como-tecnico-de-sonido.jpg',
  },
  {
    id: 16,
    title: 'Normalizar la hipoacusia',
    href: '#',
    description:
      'La normalización de las personas con problemas auditivos está recorriendo un largo camino. A través de asociaciones, instituciones, empresas del sector, pero sobre todo la verbalización y proyección...',
    date: 'Mar 16, 2020',
    datetime: '2020-03-16',
    category: { title: 'Soul&Alegría', href: '#' },
    imageUrl:
      'https://soulalegria.com/images/blog/un-dia-como-tecnico-de-sonido.jpg',
  },
];
