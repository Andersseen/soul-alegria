import { nextui } from "@nextui-org/react";
/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
    "./node_modules/@nextui-org/theme/dist/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        main: {
          50: "#fef3eb",
          100: "#ffe6db",
          200: "#ffcdc0",
          300: "#ffa79d",
          400: "#f87272",
          500: "#ff4d61",
          600: "#ff2353",
          700: "#ff0048",
          800: "#fb0057",
          900: "#de0069",
          950: "#150015",
        },
      },
    },
  },
  darkMode: "class",
  plugins: [
    nextui({
      themes: {
        light: {
          layout: {}, // light theme layout tokens
          colors: {
            background: "#fef3eb",
            foreground: "#150015",
            secondary: {
              DEFAULT: "#f87272",
              foreground: "#fef3eb",
            },
          },
        },
        dark: {
          layout: {}, // dark theme layout tokens
          colors: {
            background: "#150015",
            foreground: "#fef3eb",
            secondary: {
              DEFAULT: "#f87272",
              foreground: "#150015",
            },
            focus: "#F182F6",
          },
        },
        custom: {
          extend: "dark", // <- inherit default values from dark theme
          colors: {
            background: "#0D001A",
            foreground: "#ffffff",
            primary: {
              50: "#fef2f2",
              100: "#fee2e2",
              200: "#fecaca",
              300: "#fca5a5",
              400: "#f87171",
              500: "#ef4444",
              600: "#dc2626",
              700: "#b91c1c",
              800: "#991b1b",
              900: "#7f1d1d",
              DEFAULT: "#f87171",
              foreground: "#a21caf",
            },
            focus: "#701a75",
          },
          layout: {
            disabledOpacity: "0.3",
            radius: {
              small: "1px",
              medium: "2px",
              large: "4px",
            },
            borderWidth: {
              small: "1px",
              medium: "2px",
              large: "3px",
            },
          },
        },
      },
    }),
  ],
};
